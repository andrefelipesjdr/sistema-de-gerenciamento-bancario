import { TestBed } from '@angular/core/testing';

import { ContratacaoService } from './contratacao.service';

describe('ContratacaoService', () => {
  let service: ContratacaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContratacaoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
