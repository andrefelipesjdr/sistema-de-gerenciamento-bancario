import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContratacaoService {
  urlFindServico: string = "api/andresys/findservicos"
  urlSalvaServico: string = "api/andresys/salvarcliente"

  constructor(private _httpClient:HttpClient) {
  }

  public listaServicos():any{
    return this._httpClient.get(this.urlFindServico);
  }

  public salvar(idServico: number, idCliente: number):any{
    let params = {
      idCliente: idCliente,
      idServico: idServico
    }
    return this._httpClient.post<Array<any>>(this.urlSalvaServico, params);
  }
}
