import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransferenciaService {
  urlTransferencia: string = "api/andresys/transferencia";

  constructor(private _httpClient:HttpClient) { }

  public transferencia(idOri: number, idDest: number, valor: number):any{
    let params = {
      idOri: idOri,
      idDest: idDest,
      valor: valor
    }
    return this._httpClient.post<Array<any>>(this.urlTransferencia, params);
  }
}
