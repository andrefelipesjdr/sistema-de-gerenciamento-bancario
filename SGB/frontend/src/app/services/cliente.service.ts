import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  url: string = "api/andresys/findclientes"

  constructor(private _httpClient:HttpClient) { }

  public findClientes(){
    return this._httpClient.get(this.url);
  }
}
