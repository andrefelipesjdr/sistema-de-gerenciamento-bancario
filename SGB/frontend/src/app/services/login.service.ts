import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  urlSendLogin: string = "api/andresys/login";

  constructor(private _httpClient:HttpClient) { }

  public login(nome: string, senha: number):any{
    let params = {
      nome: nome,
      senha: senha
    }
    //return this._httpClient.get(this.urlSendLogin+'?'+nome+'/'+senha);
    //return this._httpClient.get(this.urlSendLogin);
    return this._httpClient.post<Array<any>>(this.urlSendLogin, params);
  }
}
