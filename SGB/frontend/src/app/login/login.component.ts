import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public nome: string = '';
  public senha: string = '';
  public ativo: boolean = false;
  loginForm: any;
  logando: boolean = false;
  logou: boolean = false;
  constructor(private loginService: LoginService,private router: Router ) {
    
   }

  ngOnInit(): void {
    /*this.loginService.login('eu',1).subscribe((res: any)=>{
        console.log(res);
    });*/
    if(this.router.url === '/login'){
      this.ativo = true;
    } else {
      this.ativo = false;
    }
    console.log(this.router.url);
  }

  onSubmit(){
    this.logando = true;
    console.log(this.nome + ' ' +Number(this.senha))
    this.loginService.login(this.nome,Number(this.senha)).subscribe((res: any)=>{
      console.log(res);
      this.logou = res;
      this.logando = true;
  });
  }

}
