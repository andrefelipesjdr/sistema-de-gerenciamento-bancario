import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { TransferenciaService } from '../services/transferencia.service';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.scss']
})
export class TransferenciaComponent implements OnInit {
  clienteOri: any;
  clienteDest: any;
  clientes: any = [];
  foiSalvo: boolean = false;
  valor: number = 0;
  mensagem: string = '';
  listSaldo: any[] = [];
  saldo: number = 0;
  constructor(
    @Inject(DOCUMENT) private document: any,
    private clienteService: ClienteService,
    private transfericiaService: TransferenciaService
    ) { }

  ngOnInit(): void {
    this.clienteService.findClientes().subscribe((res: any)=>{
      let list = res;
      this.listSaldo = res;
      for(let i = 0; i < list.length; i++ ){
        this.clientes.push({text: list[i].nome + ' Conta: '+list[i].conta , value: list[i].id});
      }
    });
  }
  onChange(a:any){
    console.log(this.listSaldo[this.clienteOri-1].saldo)
    this.saldo = this.listSaldo[this.clienteOri-1].saldo;
  }
  onSubmit(){
    this.valor = Number(this.document.getElementById("valor").value);
    this.transfericiaService.transferencia(this.clienteOri, this.clienteDest, this.valor).subscribe((res: any)=>{
      this.mensagem = res;
      this.foiSalvo = true;
    });
    this.foiSalvo = true
  }

}
