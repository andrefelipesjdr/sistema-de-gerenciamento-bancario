import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { ClienteService } from '../services/cliente.service';
import { ContratacaoService } from '../services/contratacao.service';


@Component({
  selector: 'app-contratacao',
  templateUrl: './contratacao.component.html',
  styleUrls: ['./contratacao.component.scss']
})
export class ContratacaoComponent implements OnInit {
  pacote: any;
  pacotes: any = [];
  cliente: any;
  clientes: any = [];
  listPac: any[] = [];
  foiSalvo: boolean = false;
  pacAtual: string = '';
  listTotPac: any[]=[];
  constructor(
    private contratacaoService: ContratacaoService,
    private clienteService: ClienteService
    ) {
  
   }

  ngOnInit(): void {
    this.contratacaoService.listaServicos().subscribe((res: any)=>{
      let list = res;
      this.listTotPac = res;
      for(let i = 0; i < list.length; i++ ){
        this.pacotes.push({text: list[i].tipo, value: list[i].id});
      }
    });
    this.clienteService.findClientes().subscribe((res: any)=>{
      let list = res;
      this.listPac = res;
      for(let i = 0; i < list.length; i++ ){
        this.clientes.push({text: list[i].nome + ' Conta: '+list[i].conta , value: list[i].id});
      }
    });
  }
  onChange(a:any){
    let num = this.listPac[this.cliente-1].servico;
    this.pacAtual = this.listTotPac[num - 1].tipo;
    console.log(this.pacote + ' ' + this.cliente);
  }
  onSubmit(){
    this.contratacaoService.salvar(this.pacote, this.cliente).subscribe((res: any)=>{
      this.foiSalvo = res;
    });
  }

}
