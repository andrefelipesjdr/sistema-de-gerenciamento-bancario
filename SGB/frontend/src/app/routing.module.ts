import {Routes} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TransacaoComponent } from './transacao/transacao.component';



export const routes:Routes =[
    {path:'',component: LoginComponent},
    {
        path:'/login',
        component:LoginComponent
    
    },
    {
        path:'/transacao',
        component:TransacaoComponent
    }
];
