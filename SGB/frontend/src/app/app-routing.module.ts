import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContratacaoComponent } from './contratacao/contratacao.component';
import { LoginComponent } from './login/login.component';
import { TransacaoComponent } from './transacao/transacao.component';
import { TransferenciaComponent } from './transferencia/transferencia.component';

const routes: Routes = [
  {path:'',component: LoginComponent},
    {
    path:'login',
    component:LoginComponent
    },
    {
        path:'menu',
        component:TransacaoComponent
    },
    {
      path:'contratacao',
      component:ContratacaoComponent
    },
    {
      path:'transferencia',
      component:TransferenciaComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
