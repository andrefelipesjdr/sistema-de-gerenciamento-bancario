package com.sistema.services.Params;

public class LoginParams{
    String nome;
    Integer senha;

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public Integer getSenha() {
        return senha;
    }
    public void setSenha(Integer senha) {
        this.senha = senha;
    }
}