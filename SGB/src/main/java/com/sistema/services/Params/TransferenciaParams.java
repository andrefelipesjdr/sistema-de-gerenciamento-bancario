package com.sistema.services.Params;

public class TransferenciaParams {
    int idOri;
    int idDest;
    Long valor;

    public int getIdOri() {
        return idOri;
    }
    public void setIdOri(int idOri) {
        this.idOri = idOri;
    }
    public int getIdDest() {
        return idDest;
    }
    public void setIdDest(int idDest) {
        this.idDest = idDest;
    }
    public Long getValor(){
        return valor;
    }
    public void setValor(Long valor){
        this.valor = valor;
    }

}