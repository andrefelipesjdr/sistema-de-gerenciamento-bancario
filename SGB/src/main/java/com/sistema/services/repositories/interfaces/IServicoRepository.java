package com.sistema.services.repositories.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistema.services.entities.Servico;

public interface IServicoRepository extends JpaRepository<Servico, Integer>{

}
