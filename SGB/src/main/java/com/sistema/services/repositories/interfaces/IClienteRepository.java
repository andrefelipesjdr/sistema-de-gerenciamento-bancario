package com.sistema.services.repositories.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistema.services.entities.Cliente;

public interface IClienteRepository extends JpaRepository<Cliente, Integer>{

}
