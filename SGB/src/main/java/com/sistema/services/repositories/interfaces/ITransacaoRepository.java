package com.sistema.services.repositories.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistema.services.entities.Transacao;

public interface ITransacaoRepository extends JpaRepository<Transacao, Integer>{

}
