package com.sistema.services.repositories.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sistema.services.entities.Funcionario;

public interface IFuncionarioRepository extends JpaRepository<Funcionario, Integer> {

}
