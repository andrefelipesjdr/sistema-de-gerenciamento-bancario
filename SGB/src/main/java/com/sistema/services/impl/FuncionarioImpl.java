package com.sistema.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistema.services.entities.Funcionario;
import com.sistema.services.repositories.interfaces.IFuncionarioRepository;

@Service
public class FuncionarioImpl {
	
	@Autowired
	IFuncionarioRepository funcionarioRepository;
	
	public List<Funcionario>findAll(){
		return funcionarioRepository.findAll();
	}
	public boolean loginFuncionario(String nome, Integer senha){
		List<Funcionario> funcionarios = this.findAll();
		for(Funcionario funcionario: funcionarios){
			if(funcionario.getNome().equals(nome) && funcionario.getSenha() == senha){
				return true;
			}
		}
		return false;
	}

}
