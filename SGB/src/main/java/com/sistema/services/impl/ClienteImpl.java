package com.sistema.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistema.services.entities.Cliente;
import com.sistema.services.repositories.interfaces.IClienteRepository;

@Service
public class ClienteImpl {
	
	@Autowired
	IClienteRepository clienteRepository;
	
	public List<Cliente>findAll(){
		return clienteRepository.findAll();
	}

	public boolean salvarcliente(Cliente cliente){
		this.clienteRepository.save(cliente);
		return true;
	}

	public Optional<Cliente> findById(Integer id){
		if(id != null){
			return this.clienteRepository.findById(id);
		}
		return null;
	}
}
