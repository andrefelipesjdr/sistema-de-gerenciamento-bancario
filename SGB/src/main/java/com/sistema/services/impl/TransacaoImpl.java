package com.sistema.services.impl;

import java.util.Optional;

import com.sistema.services.entities.Cliente;
import com.sistema.services.entities.Transacao;
import com.sistema.services.repositories.interfaces.ITransacaoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransacaoImpl {

    @Autowired
    ITransacaoRepository transacaoRepository;
    @Autowired
    ClienteImpl clienteImpl;

    public String transferencia(int idOri, int idDest, Long valor){
        Optional<Cliente> oriCliente = this.clienteImpl.findById(idOri);
        Optional<Cliente> destCliente = this.clienteImpl.findById(idDest);
        if(oriCliente == null){
            return "Conta origem nao existe";
        }
        if(destCliente == null){
            return "Conta destino nao existe";
        }
        Long saldo = oriCliente.get().getSaldo();
        saldo = saldo - valor;
        if(saldo > 0){
            Long saldoDest = destCliente.get().getSaldo();
            oriCliente.get().setSaldo(saldo);
            destCliente.get().setSaldo(saldoDest + valor);
            this.clienteImpl.salvarcliente(oriCliente.get());
            this.clienteImpl.salvarcliente(destCliente.get());
            //this.salvaTransacao(idOri,idDest, valor);
            return "Tranferencia feita " + oriCliente.get().getNome() + " -> " + destCliente.get().getNome();
        } else {
            return "Sem saldo!";
        }
    }
    public void salvaTransacao(int idOri, int idDest, Long valor){
        Transacao transacao = new Transacao();
        transacao.setContaOri(idOri);
        transacao.setContaDest(idDest);
        transacao.setValor(valor);
        transacao.setTipo("Transferencia");
        this.transacaoRepository.save(transacao);
    }

}
