package com.sistema.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistema.services.entities.Cliente;
import com.sistema.services.entities.Servico;
import com.sistema.services.repositories.interfaces.IServicoRepository;
@Service
public class ServicoImpl {
	
	@Autowired
	IServicoRepository servicorepository;
	
	@Autowired
	ClienteImpl clienteImpl;
	
	public List<Servico> findAll(){
		return servicorepository.findAll();
	}
	public boolean salvarcliente(int idCliente, int idServico){
		List<Cliente> clients = this.clienteImpl.findAll();
		Cliente clienteParaSalvar = null;
		for(Cliente cliente : clients){
			if(cliente.getId() == idCliente){
				cliente.setServico(idServico);
				clienteParaSalvar = cliente;
			}
		}
		if(clienteParaSalvar != null){
			return this.clienteImpl.salvarcliente(clienteParaSalvar);
		}
		return false;
	}

}
