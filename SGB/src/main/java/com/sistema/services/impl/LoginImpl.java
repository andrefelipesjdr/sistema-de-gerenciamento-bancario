package com.sistema.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginImpl {
	@Autowired
	FuncionarioImpl funcionarioImpl;

	public boolean verificaPermissao(String nome, Integer senha) {
		if(nome.equals(null) || senha == null){
			return false;
		} 
		return this.funcionarioImpl.loginFuncionario(nome, senha);
	}

}
