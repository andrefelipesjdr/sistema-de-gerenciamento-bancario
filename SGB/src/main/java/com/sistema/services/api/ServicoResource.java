package com.sistema.services.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.sistema.services.impl.*;
import org.springframework.http.MediaType;


import java.util.List;

import com.sistema.services.Params.ClienteParams;
import com.sistema.services.entities.*;

@RestController
public class ServicoResource {
	
	@Autowired
	ServicoImpl servicoImpl;
	
	@RequestMapping(value="findservicos",  method=RequestMethod.GET)
	public List<Servico> findAll() {
		return servicoImpl.findAll();
	}

	@RequestMapping(value="salvarcliente", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean salvarcliente(@RequestBody ClienteParams clienteParams) {
		return servicoImpl.salvarcliente(clienteParams.getIdCliente(), clienteParams.getIdServico());
	}

}
