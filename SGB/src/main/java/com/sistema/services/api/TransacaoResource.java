package com.sistema.services.api;
import org.springframework.beans.factory.annotation.Autowired;

import com.sistema.services.Params.TransferenciaParams;
import com.sistema.services.impl.TransacaoImpl;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransacaoResource {
    
    @Autowired
    TransacaoImpl transacaoImpl;

    @RequestMapping(value="transferencia", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> transferencia(@RequestBody TransferenciaParams transferenciaParams) {
		return ResponseEntity.ok(transacaoImpl.transferencia(transferenciaParams.getIdOri(), transferenciaParams.getIdDest(), transferenciaParams.getValor()));
	}

}
