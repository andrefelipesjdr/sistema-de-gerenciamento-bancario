package com.sistema.services.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.sistema.services.entities.Cliente;
import com.sistema.services.impl.ClienteImpl;
@RestController
public class ClienteResource {
    
    @Autowired 
    ClienteImpl clienteImpl;

    @RequestMapping(value="findclientes", method=RequestMethod.GET)
    public List<Cliente>findAll(){
        return this.clienteImpl.findAll();
    }
}
