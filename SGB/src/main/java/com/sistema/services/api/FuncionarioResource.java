package com.sistema.services.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.sistema.services.entities.Funcionario;
import com.sistema.services.impl.FuncionarioImpl;

@RestController
public class FuncionarioResource {
	
	@Autowired 
	FuncionarioImpl funcionarioimpl;
	
	@RequestMapping(value="findAll", method=RequestMethod.GET )
	public List<Funcionario>findAll(){
		return funcionarioimpl.findAll();
	}
}
