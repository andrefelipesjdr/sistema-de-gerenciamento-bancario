package com.sistema.services.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sistema.services.Params.*;
import com.sistema.services.impl.LoginImpl;

@RestController
public class LoginResource {
	
	@Autowired
	LoginImpl loginimpl;
	
	@RequestMapping(value="login", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean login(@RequestBody LoginParams loginParams) {
		return loginimpl.verificaPermissao(loginParams.getNome(), loginParams.getSenha());
	}

}
