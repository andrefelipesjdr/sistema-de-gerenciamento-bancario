package com.sistema.services.entities;

import javax.persistence.Entity;

@Entity
public class Transacao extends AbstractEntity {
	
	private String tipo;
	
	private int contaOri;
	
	private int contaDest;
	
	private long valor;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getContaOri() {
		return contaOri;
	}

	public void setContaOri(int contaOri) {
		this.contaOri = contaOri;
	}

	public int getContaDest() {
		return contaDest;
	}

	public void setContaDest(int contaDest) {
		this.contaDest = contaDest;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}
}
