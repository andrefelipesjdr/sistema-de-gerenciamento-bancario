package com.sistema.services.entities;

import javax.persistence.Entity;

@Entity
public class Servico extends AbstractEntity {
	
	private String tipo;
	
	private boolean confirmacao;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isConfirmacao() {
		return confirmacao;
	}

	public void setConfirmacao(boolean confirmacao) {
		this.confirmacao = confirmacao;
	}

}
