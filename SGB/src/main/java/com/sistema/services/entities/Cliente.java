package com.sistema.services.entities;

import javax.persistence.Entity;

@Entity
public class Cliente extends AbstractEntity {
	
	private String nome;
	
	private String conta;
	
	private int senha;

	private int servico;

	private Long saldo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public int getSenha() {
		return senha;
	}

	public void setSenha(int senha) {
		this.senha = senha;
	}

	public int getServico(){
		return this.servico;
	}

	public void setServico(int servico){
		this.servico = servico;
	}

	public Long getSaldo(){
		return this.saldo;
	}

	public void setSaldo(Long saldo){
		this.saldo = saldo;
	}
	

}
