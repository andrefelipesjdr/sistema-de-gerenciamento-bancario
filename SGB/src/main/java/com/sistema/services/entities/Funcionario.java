package com.sistema.services.entities;

import javax.persistence.Entity;

@Entity
public class Funcionario extends AbstractEntity {
	
	private String nome;
	
	private int nivel;
	
	private int senha;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public int getSenha() {
		return senha;
	}
	public void setSenha(int senha) {
		this.senha = senha;
	}
}
